/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.uniminuto.pa.DAOs;

import co.edu.uniminuto.pa.DTOs.Persona;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nixoduaa
 */
public class PersonaDAO {
    
    
    
    public boolean crearPersona(Persona p, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando crearPersona...");
            
            pstmt = con.prepareStatement("INSERT INTO cliente "
                    + " VALUES (?,?,?,?,?,?,?,?,?,?)");
            
            //pstmt.setInt(1,p.getId());
            pstmt.setString(1, p.getIdentificacion());
            pstmt.setString(2, p.getNombre1());
            pstmt.setString(3, p.getNombre2());
            pstmt.setString(4, p.getApellido1());
            pstmt.setString(5, p.getApellido2());
            pstmt.setString(6, p.getGenero());
            pstmt.setString(7, p.getTipoP());
            pstmt.setString(8, p.getfNacimiento());
            pstmt.setString(9, p.getTelef());
            pstmt.setString(10, p.getEmail());
            
            
            
            pstmt.execute();
            
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }

    public ArrayList<Persona> consultarPersona(Persona p, Connection con)
    {
        
        ArrayList<Persona> datos = new ArrayList();
        
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
        
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select cedula, nombre1,nombre2,"
                    + "apellido1, apellido2, genero, tipo_persona, fecha, telefono, email from cliente "
                    + " where cedula='" + p.getIdentificacion()+"'"
                    + " AND nombre1='"+p.getNombre1()+"'");
            
            while (rs.next())
            { 
                Persona per = new Persona();
                per.setIdentificacion(rs.getString(1));
                per.setNombre1(rs.getString(2));
                per.setNombre2(rs.getString(3));
                per.setApellido1(rs.getString(4));
                per.setApellido2(rs.getString(5));
                per.setGenero(rs.getString(6));
                per.setTipoP(rs.getString(7));
                per.setfNacimiento(rs.getString(8));
                per.setTelef(rs.getString(9));
                per.setEmail(rs.getString(10));
                datos.add(per);
                
            }
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona fin..." + datos.size());
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    
         
    public boolean editarPersona(Persona p, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando editarPersona...");
            
            pstmt = con.prepareStatement("UPDATE cliente "
                    + " SET cedula=? , nombre1=? , nombre2=?"
                    + " where cedula=?");
                        
            pstmt.setString(1, p.getIdentificacion());
            pstmt.setString(2, p.getNombre1());
            pstmt.setString(3, p.getNombre2());
            pstmt.setString(4, p.getIdentificacion());
            
            pstmt.executeUpdate();
            
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }
}
