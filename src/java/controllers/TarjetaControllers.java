/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

/**
 *
 * @author NixonD
 */

import co.edu.uniminuto.pa.DAOs.PersonaDAO;
import co.edu.uniminuto.pa.DAOs.ProductoDAO;
import co.edu.uniminuto.pa.DAOs.TarjetaDAO;
import co.edu.uniminuto.pa.DTOs.Lista;
import co.edu.uniminuto.pa.DTOs.Persona;
import co.edu.uniminuto.pa.DTOs.Producto;
import co.edu.uniminuto.pa.DTOs.Tarjeta;
import co.edu.uniminuto.pa.bds.MySqlDataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
 
@Controller
@RequestMapping("/")
public class TarjetaControllers {
 /*
    @RequestMapping(method = RequestMethod.GET)
    public String helloWorld(ModelMap modelMap) {
        System.out.println("personaCrear");
        modelMap.put("mensajePersona", "Pase por el controller de Persona");
        return "persona_crear";
    }
    */

    @RequestMapping(method = RequestMethod.GET,value = "tarjetaCrear.htm")
    public String home(HttpServletRequest req, SessionStatus status,ModelMap model) {

        TarjetaDAO pDao = new TarjetaDAO();
        ProductoDAO dao = new ProductoDAO();    
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");
        Tarjeta p = new Tarjeta();
        Producto pr = new Producto();
        
        List<Tarjeta> datosC = pDao.crearSolicitudTarjeta(p, MySqlDataSource.getConexionBD());
        List<Producto> datosPr = dao.crearSolicitudProducto(pr, MySqlDataSource.getConexionBD());
        model.put("lista",new Lista());
        model.put("listaPersonas", datosC);
        model.put("listaProductos", datosPr);
        
        if (datosC.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datosC.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        return "tarjeta_crear";
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "tarjetaCrearForm.htm")
    public String processSubmit1(Lista lista,ModelMap model) 
    {
        TarjetaDAO tDao = new TarjetaDAO();
        Tarjeta t = new Tarjeta();
        Producto pr = new Producto();
        model.put("lista",new Lista());
        model.addAttribute("lista", lista);
        String cliente = lista.getCliente();
        String[] arrayCliente = cliente.split(" ");
        String nombre1 = arrayCliente[0];
        String apellido1 = arrayCliente[1];
        String producto = lista.getProducto();
        t.setNombre1(nombre1);
        t.setApellido1(apellido1);
        pr.setNombre(producto);
        String id=null,ident=null;
        if("visa mastercard".equals(producto)){id="1";}else if("mastercard black".equals(producto)){id="2";}
        else if("visa".equals(producto)){id="3";}else if("diners club".equals(producto)){id="4";}
        else if("mastercard".equals(producto)){id="5";}
        pr.setId(id);
        List<Tarjeta> datosC = tDao.consultarPersona(t, MySqlDataSource.getConexionBD());
        for (Tarjeta obj : datosC) {
            ident=obj.getIdentificacion();
        }
        t.setIdentificacion(ident);
        System.out.println("Cliente: "+nombre1+" Apellido: "+apellido1+" Producto: "+producto);
        boolean insert = tDao.crearSolicitud(t,pr,MySqlDataSource.getConexionBD());
        Logger.getLogger(PersonaControllers.class.getName()).log(Level.SEVERE, null, "Registrar " + insert);
        if (insert)
            model.put("mensaje", "El registro fue creado satisfactoriamente!!!");
        else
            model.put("mensaje", "El registro NO fue creado, consulte con el administrador...");
        return "tarjeta_crear";
    }     

@RequestMapping(method = RequestMethod.GET, value = "tarjetaConsultar.htm")
    public String processSubmit2(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {      
        TarjetaDAO pDao = new TarjetaDAO();
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");
        Tarjeta p = new Tarjeta();
        
        List<Tarjeta> datosC = pDao.crearSolicitudTarjeta(p, MySqlDataSource.getConexionBD());
        model.put("lista",new Lista());
        model.put("listaPersonas", datosC);
        if (datosC.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datosC.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        Logger.getLogger(PersonaControllers.class.getName()).log(Level.INFO, "Ejecutando processSubmit2...");
        return "tarjeta_consultar";
    } 
    
@RequestMapping(method = RequestMethod.POST, value = "tarjetaConsultarForm.htm")
    public String processSubmit3(Lista lista,ModelMap model) 
    {
        
        TarjetaDAO tDao = new TarjetaDAO();
        Tarjeta t = new Tarjeta();
        model.put("lista",new Lista());
        model.addAttribute("lista", lista);
        String cliente = lista.getCliente();
        String[] arrayCliente = cliente.split(" ");
        String nombre1 = arrayCliente[0];
        String apellido1 = arrayCliente[1];
        t.setNombre1(nombre1);
        t.setApellido1(apellido1);
        String ident=null;
        List<Tarjeta> datosC = tDao.consultarPersona(t, MySqlDataSource.getConexionBD());
        for (Tarjeta obj : datosC) {
            ident=obj.getIdentificacion();
        }
        t.setIdentificacion(ident);
        List<Tarjeta> datos = tDao.tarjetaConsultar(t,MySqlDataSource.getConexionBD());
        model.put("datos",datos);
        return "tarjeta_consultar";
    }     
    
    @RequestMapping(method = RequestMethod.GET, value = "tarjetaEditar.htm")
    public String processSubmit6(Lista lista,ModelMap model) 
    {
        TarjetaDAO pDao = new TarjetaDAO();
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");
        Tarjeta p = new Tarjeta();
        
        List<Tarjeta> datosC = pDao.crearSolicitudTarjeta(p, MySqlDataSource.getConexionBD());
        model.put("lista",new Lista());
        model.put("listaPersonas", datosC);
        if (datosC.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datosC.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        Logger.getLogger(PersonaControllers.class.getName()).log(Level.INFO, "Ejecutando processSubmit2...");
        return "tarjeta_editar";
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "tarjetaEditarForm.htm")
    public String processSubmit4(Lista lista,ModelMap model) 
    {      
        TarjetaDAO tDao = new TarjetaDAO();
        Tarjeta t = new Tarjeta();
        model.put("lista",new Lista());
        model.addAttribute("lista", lista);
        String cliente = lista.getCliente();
        String[] arrayCliente = cliente.split(" ");
        String nombre1 = arrayCliente[0];
        String apellido1 = arrayCliente[1];
        t.setNombre1(nombre1);
        t.setApellido1(apellido1);
        String ident=null;
        List<Tarjeta> datosC = tDao.consultarPersona(t, MySqlDataSource.getConexionBD());
        for (Tarjeta obj : datosC) {
            ident=obj.getIdentificacion();
        }
        t.setIdentificacion(ident);
        List<Tarjeta> datos = tDao.tarjetaConsultar(t,MySqlDataSource.getConexionBD());
        model.put("datos",datos);
        return "tarjeta_editar";
    } 
    
@RequestMapping(method = RequestMethod.POST, value = "tarjetaEditarForm2.htm")
    public String processSubmit5(Lista lista,ModelMap model) 
    {
        
        TarjetaDAO tDao = new TarjetaDAO();
        Tarjeta t = new Tarjeta();
        Producto pr = new Producto();
        model.put("lista",new Lista());
        model.addAttribute("lista", lista);
        String cliente = lista.getCliente();
        String[] arrayCliente = cliente.split(" ");
        String nombre1 = arrayCliente[0];
        String apellido1 = arrayCliente[1];
        String producto = lista.getProducto();
        t.setNombre1(nombre1);
        t.setApellido1(apellido1);
        pr.setNombre(producto);
        String id=null,ident=null;
        if("visa mastercard".equals(producto)){id="1";}else if("mastercard black".equals(producto)){id="2";}
        else if("visa".equals(producto)){id="3";}else if("diners club".equals(producto)){id="4";}
        else if("mastercard".equals(producto)){id="5";}
        pr.setId(id);
        List<Tarjeta> datosC = tDao.consultarPersona(t, MySqlDataSource.getConexionBD());
        for (Tarjeta obj : datosC) {
            ident=obj.getIdentificacion();
        }
        t.setIdentificacion(ident);
        System.out.println("Cliente: "+nombre1+" Apellido: "+apellido1+" Producto: "+producto);
        boolean insert = tDao.crearSolicitud(t,pr,MySqlDataSource.getConexionBD());
        Logger.getLogger(PersonaControllers.class.getName()).log(Level.SEVERE, null, "Registrar " + insert);
        if (insert)
            model.put("mensaje", "El registro fue creado satisfactoriamente!!!");
        else
            model.put("mensaje", "El registro NO fue creado, consulte con el administrador...");
        return "tarjeta_editar";
        
    }       
}