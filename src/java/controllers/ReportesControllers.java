/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

/**
 *
 * @author NixonD
 */

import co.edu.uniminuto.pa.DAOs.PersonaDAO;
import co.edu.uniminuto.pa.DAOs.ProductoDAO;
import co.edu.uniminuto.pa.DAOs.ReportesDAO;
import co.edu.uniminuto.pa.DAOs.TarjetaDAO;
import co.edu.uniminuto.pa.DTOs.Persona;
import co.edu.uniminuto.pa.DTOs.Producto;
import co.edu.uniminuto.pa.DTOs.Reportes;
import co.edu.uniminuto.pa.DTOs.Tarjeta;
import co.edu.uniminuto.pa.bds.MySqlDataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
 
@Controller
@RequestMapping("/")
public class ReportesControllers {
 
    @RequestMapping(method = RequestMethod.GET,value = "solicitudAprobada.htm")
    public String home(HttpServletRequest req, SessionStatus status,ModelMap model) {

        ReportesDAO pDao = new ReportesDAO();    
        Logger.getLogger(ReportesDAO.class.getName()).log(Level.INFO, "Ejecutando reportes dao...");
        Reportes p = new Reportes();
        
        List<Reportes> datosC = pDao.solicitudAprobada(p, MySqlDataSource.getConexionBD());  
        model.put("listaProductos", datosC);
        if (datosC.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datosC.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        return "solicitud_aprobada";
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "reportes.htm")
    public String processSubmit1(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {
       
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");
        return "solicitud_aprobada";
    }
    
    @RequestMapping(method = RequestMethod.GET,value = "solicitudRechazada.htm")
    public String submit3(HttpServletRequest req, SessionStatus status,ModelMap model) {

        ReportesDAO pDao = new ReportesDAO();    
        Logger.getLogger(ReportesDAO.class.getName()).log(Level.INFO, "Ejecutando reportes dao...");
        Reportes p = new Reportes();
        
        List<Reportes> datosC = pDao.solicitudRechazada(p, MySqlDataSource.getConexionBD());  
        model.put("listaProductos", datosC);
        if (datosC.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datosC.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        return "solicitud_rechazada";
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "reportes2.htm")
    public String processSubmit4(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");
        return "solicitud_rechazada";
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "solicitudCliente.htm")
    public String processSubmit5(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {
        return "solicitud_persona";
    }    
    
    @RequestMapping(method = RequestMethod.POST, value = "solicitudPersonaForm.htm")
    public String processSubmit6(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {
        ReportesDAO pDao = new ReportesDAO();
            
        Logger.getLogger(ReportesDAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");

        String ident = req.getParameter("identificacion");
        Reportes p = new Reportes();
        System.out.println(ident);
        p.setCedula(ident);
            
        List<Reportes> datos = pDao.solicitudPersona(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(PersonaControllers.class.getName()).log(Level.SEVERE, null, "Consultar + " + ident + "-" + datos.size());
        
        model.put("listaPersonas", datos);
        if (datos.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datos.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        return "solicitud_persona";
    }   
}